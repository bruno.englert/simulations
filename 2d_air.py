#                              .-----.
#                             /7  .  (
#                            /   .-.  \
#                           /   /   \  \
#                          / `  )   (   )
#                         / `   )   ).  \
#                       .'  _.   \_/  . |
#      .--.           .' _.' )`.        |
#     (    `---...._.'   `---.'_)    ..  \
#      \            `----....___    `. \  |
#       `.           _ ----- _   `._  )/  |
#         `.       /"  \   /"  \`.  `._   |
#           `.    ((O)` ) ((O)` ) `.   `._\
#             `-- '`---'   `---' )  `.    `-.
#                /                  ` \      `-.
#              .'                      `.       `.
#             /                     `  ` `.       `-.
#      .--.   \ ===._____.======. `    `   `. .___.--`     .''''.
#     ' .` `-. `.                )`. `   ` ` \          .' . '  8)
#    (8  .  ` `-.`.               ( .  ` `  .`\      .'  '    ' /
#     \  `. `    `-.               ) ` .   ` ` \  .'   ' .  '  /
#      \ ` `.  ` . \`.    .--.     |  ` ) `   .``/   '  // .  /
#       `.  ``. .   \ \   .-- `.  (  ` /_   ` . / ' .  '/   .'
#         `. ` \  `  \ \  '-.   `-'  .'  `-.  `   .  .'/  .'
#           \ `.`.  ` \ \    ) /`._.`       `.  ` .  .'  /
#            |  `.`. . \ \  (.'               `.   .'  .'
#         __/  .. \ \ ` ) \                     \.' .. \__
#  .-._.-'     '"  ) .-'   `.                   (  '"     `-._.--.
# (_________.-====' / .' /\_)`--..__________..-- `====-. _________)
#                  (.'(.'

import tensorflow as tf
import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
import cv2
from tqdm import tqdm
import scipy
from enum import Enum

np.set_printoptions(formatter={'float': '{: 0.6f}'.format}, linewidth=500)
DEBUG = False


class SolverType(Enum):
    NUMPY = 0
    GPU = 1
    LU = 2


SOLVER_TYPE = SolverType.LU


def B1(x, y):
    return (1. - x) * (1. - y) / 4.


def B2(x, y):
    return (1. + x) * (1. - y) / 4.


def B3(x, y):
    return (1. + x) * (1. + y) / 4.


def B4(x, y):
    return (1. - x) * (1. + y) / 4.


def B1_dx(y):
    return (-1. + y) / 4.


def B2_dx(y):
    return (1. - y) / 4.


def B3_dx(y):
    return (1. + y) / 4.


def B4_dx(y):
    return (-1. - y) / 4.


def B1_dy(x):
    return (-1. + x) / 4.


def B2_dy(x):
    return (-1. - x) / 4.


def B3_dy(x):
    return (1. + x) / 4.


def B4_dy(x):
    return (1. - x) / 4.


def A1(x):
    return (1. - x) / 2.


def A2(x):
    return (1. + x) / 2.


if __name__ == "__main__":
    Nh = 120
    Nw = 120
    Lh = 1.
    Lw = 1.

    Hh = Lh / Nh
    Hw = Lw / Nw

    r0 = 1.225
    w = 2000
    c = 343

    dtype = np.float32
    M = np.zeros((Nh * Nw, Nh * Nw), dtype)
    K = np.zeros((Nh * Nw, Nh * Nw), dtype)
    A = np.zeros((Nh * Nw, Nh * Nw), dtype)

    Me = np.zeros((4, 4), dtype)

    base_func = [B1, B2, B3, B4]
    for i in range(4):
        for k in range(4):
            x, y = sp.symbols("x y")
            f = base_func[i](x, y) * base_func[k](x, y) * Hh * Hw / 4.
            Me[i, k] += sp.integrate(f, (x, -1, 1), (y, -1, 1))
    print(Me)
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
    Ke = np.zeros((4, 4), dtype)

    Jakobi = np.asarray([[Hw, 0],
                         [0, Hh]], dtype=dtype)
    Jakobi_inv = np.asarray([[1. / Hw, 0],
                             [0, 1. / Hh]], dtype=dtype)
    Jakobi_inv_tans = np.asarray([[1. / Hw, 0],
                                  [0, 1. / Hh]], dtype=dtype)

    base_func_dx = [B1_dx, B2_dx, B3_dx, B4_dx]
    base_func_dy = [B1_dy, B2_dy, B3_dy, B4_dy]
    for i in range(4):
        for k in range(4):
            x, y = sp.symbols("x y")

            f = ((base_func_dx[i](y) * base_func_dx[k](y) / (Hw * Hw) + base_func_dy[i](x) * base_func_dy[k](x) / (
                    Hh * Hh)) * (Hw * Hh / 4.)) / 4.
            Ke[i, k] = sp.integrate(f, (x, -1, 1), (y, -1, 1))
    print(Ke)
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
    boundary_base_func = [A1, A2]
    Ae = np.zeros((2, 2), dtype)
    jacobi_det_length = np.sqrt(Hh * Hh / 4. + Hw * Hw / 4.)
    for i in range(2):
        for k in range(2):
            x = sp.symbols("x")
            f = boundary_base_func[i](x) * boundary_base_func[k](x) * jacobi_det_length
            Ae[i, k] = sp.integrate(f, (x, -1, 1))
    print(Ae)

    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")

    for i in range(Nh - 1):
        for k in range(Nw - 1):
            one = (i * Nw + k)
            two = (i * Nw + 1 + k)
            three = ((i + 1) * Nw + k)
            four = ((i + 1) * Nw + k + 1)

            index_coords = [one, two, three, four]

            for l in range(4):
                for m in range(4):
                    M[index_coords[l], index_coords[m]] += Me[l, m]
                    K[index_coords[l], index_coords[m]] += Ke[l, m]
    ########################################################################
    for i in range(Nw - 1):
        one = (i)
        two = (i + 1)

        index_coords = [one, two]
        for l in range(2):
            for m in range(2):
                A[index_coords[l], index_coords[m]] += Ae[l, m]

    for i in range(Nw - 1):
        one = ((Nh - 1) * Nw + i)
        two = ((Nh - 1) * Nw + i + 1)

        index_coords = [one, two]

        for l in range(2):
            for m in range(2):
                A[index_coords[l], index_coords[m]] += Ae[l, m]

    for i in range(Nh - 1):
        one = (i * (Nw))
        two = ((i + 1) * (Nw))

        index_coords = [one, two]

        for l in range(2):
            for m in range(2):
                A[index_coords[l], index_coords[m]] += Ae[l, m]

    for i in range(Nh - 1):
        one = (i * (Nw) + Nh - 1)
        two = ((i + 1) * (Nw) + Nh - 1)

        index_coords = [one, two]
        for l in range(2):
            for m in range(2):
                A[index_coords[l], index_coords[m]] += Ae[l, m]

    M = M * r0
    K = K * r0 * c * c
    A = A * ((r0 * c) ** 2)

    if DEBUG:
        plt.imshow(M)
        plt.show()
        plt.imshow(K)
        plt.show()
        plt.imshow(A)
        plt.show()

    dt = 0.00005
    total_time = 0.03
    beta = 1 / 4.
    gamma = 1 / 2.

    img_height = 500
    img_width = 500
    video = cv2.VideoWriter("./2d_air.mp4", cv2.VideoWriter_fourcc(*'mp4v'), 60., (img_width, img_height))
    if SOLVER_TYPE == SolverType.NUMPY:
        p = np.ones((Nh * Nw,), dtype=dtype) * 0.00
        p_d = np.ones((Nh * Nw,), dtype=dtype) * 0.0
        p_dd = np.zeros((Nh * Nw,), dtype=dtype)
        p[int(Nw * Nh // 2 + Nh // 4)] = 10.1
        v = np.zeros((Nh * Nw,), dtype=dtype)

        for i in tqdm(range(int(total_time / dt))):
            # Prediction
            p_1_cap = p + dt * p_d + (dt ** 2) / 2. * (1. - 2. * beta) * p_dd
            p_1_cap_d = p_d + dt * (1. - gamma) * p_dd
            # Solution
            p_dd = np.linalg.solve(M + (dt ** 2) * beta * K, -np.matmul(K, p_1_cap))  # np.matmul(A, v))
            # Correction
            p = p_1_cap + (dt ** 2) * beta * p_dd
            p_d = p_1_cap_d + dt * gamma * p_dd

            img = np.reshape(p, newshape=(Nw, Nh))
            # print(img)

            img_normed = (img - np.min(img)) / (np.max(img) - np.min(img)) * 255.

            img = img_normed.astype(np.uint8)
            img = cv2.resize(img, (500, 500), interpolation=cv2.INTER_LINEAR)
            cv2.imshow("asd", img)
            print(i)
            cv2.waitKey(10)
    elif SOLVER_TYPE == SolverType.GPU:
        p = np.ones((Nh * Nw, 1), dtype=dtype) * 0.00
        p_d = np.ones((Nh * Nw, 1), dtype=dtype) * 0.0
        p_dd = np.zeros((Nh * Nw, 1), dtype=dtype)
        p[int(Nw * Nh // 2 + Nh // 4)] = 10.1
        v = np.zeros((Nh * Nw, 1), dtype=dtype)
        max_p = p.max()

        p = tf.constant(p)
        p_d = tf.constant(p_d)
        P_dd = tf.constant(p_dd)
        A_tf = tf.constant(M + (dt ** 2) * beta * K)
        K = tf.constant(K)

        for i in tqdm(range(int(total_time / dt))):
            # Prediction
            p_1_cap = p + dt * p_d + (dt ** 2) / 2. * (1. - 2. * beta) * p_dd
            p_1_cap_d = p_d + dt * (1. - gamma) * p_dd
            # Solution
            b_tf = -tf.matmul(K, p_1_cap)
            p_dd = tf.linalg.solve(A_tf, b_tf, adjoint=False, name=None)
            # Correction
            p = p_1_cap + (dt ** 2) * beta * p_dd
            p_d = p_1_cap_d + dt * gamma * p_dd

            img = np.reshape(p.numpy(), newshape=(Nw, Nh))
            # img_normed = (img - np.min(img)) / (np.max(img)-np.min(img)) * 255.
            img_normed = (img - max_p) / (2 * max_p) * 255.
            img = img_normed.astype(np.uint8)
            img = cv2.resize(img, (250, 250), interpolation=cv2.INTER_LINEAR)
            cv2.imshow("asd", img)
            print(i)
            cv2.waitKey(1)
    elif SOLVER_TYPE == SolverType.LU:
        p = np.ones((Nh * Nw,), dtype=dtype) * 0.00
        p_d = np.ones((Nh * Nw,), dtype=dtype) * 0.0
        p_dd = np.zeros((Nh * Nw,), dtype=dtype)
        p[int(Nw * Nh // 2 + Nh // 4)] = 50.
        v = np.zeros((Nh * Nw,), dtype=dtype)
        max_p = p.max()

        lu = scipy.linalg.lu_factor(M + (dt ** 2) * beta * K)
        for i in tqdm(range(int(total_time / dt))):
            # Prediction
            p_1_cap = p + dt * p_d + (dt ** 2) / 2. * (1. - 2. * beta) * p_dd
            p_1_cap_d = p_d + dt * (1. - gamma) * p_dd
            # Solution
            b_tf = -np.matmul(K, p_1_cap)
            p_dd = scipy.linalg.lu_solve(lu, b_tf)
            # Correction
            p = p_1_cap + (dt ** 2) * beta * p_dd
            p_d = p_1_cap_d + dt * gamma * p_dd

            img = np.reshape(p, newshape=(Nw, Nh, 1))
            # img_normed = (img - np.min(img)) / (np.max(img) - np.min(img)) * 255.
            # max_p = max(np.abs(np.min(img)), np.max(img))
            # img_normed = (img / max_p) * 255.

            img_normed = img * 100. + 100
            r = np.copy(img_normed)
            g = np.zeros(img_normed.shape)
            b = np.copy(img_normed)

            b[img_normed >= 0] = 0
            b *= -1
            r[img_normed < 0] = 0

            img = np.concatenate([b, g, r], axis=-1)
            img = img.astype(np.uint8)

            img = cv2.resize(img, (img_width, img_height), interpolation=cv2.INTER_LINEAR)
            video.write(img)
            cv2.imshow("asd", img)
            cv2.waitKey(1)
    else:
        raise Exception("Unsupported solver: {}".format(SOLVER_TYPE))
    cv2.destroyAllWindows()
    video.release()
