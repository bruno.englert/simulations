#                              .-----.
#                             /7  .  (
#                            /   .-.  \
#                           /   /   \  \
#                          / `  )   (   )
#                         / `   )   ).  \
#                       .'  _.   \_/  . |
#      .--.           .' _.' )`.        |
#     (    `---...._.'   `---.'_)    ..  \
#      \            `----....___    `. \  |
#       `.           _ ----- _   `._  )/  |
#         `.       /"  \   /"  \`.  `._   |
#           `.    ((O)` ) ((O)` ) `.   `._\
#             `-- '`---'   `---' )  `.    `-.
#                /                  ` \      `-.
#              .'                      `.       `.
#             /                     `  ` `.       `-.
#      .--.   \ ===._____.======. `    `   `. .___.--`     .''''.
#     ' .` `-. `.                )`. `   ` ` \          .' . '  8)
#    (8  .  ` `-.`.               ( .  ` `  .`\      .'  '    ' /
#     \  `. `    `-.               ) ` .   ` ` \  .'   ' .  '  /
#      \ ` `.  ` . \`.    .--.     |  ` ) `   .``/   '  // .  /
#       `.  ``. .   \ \   .-- `.  (  ` /_   ` . / ' .  '/   .'
#         `. ` \  `  \ \  '-.   `-'  .'  `-.  `   .  .'/  .'
#           \ `.`.  ` \ \    ) /`._.`       `.  ` .  .'  /
#            |  `.`. . \ \  (.'               `.   .'  .'
#         __/  .. \ \ ` ) \                     \.' .. \__
#  .-._.-'     '"  ) .-'   `.                   (  '"     `-._.--.
# (_________.-====' / .' /\_)`--..__________..-- `====-. _________)
#                  (.'(.'

import numpy as np
import sympy as sp
import matplotlib.pyplot as plt

np.set_printoptions(formatter={'float': '{: 0.3f}'.format}, linewidth=500)

if __name__ == "__main__":
    N = 300
    L = 5

    k = np.asarray([[1, -1], [-1, 1]])
    m = np.asarray([[2, 1], [1, 2]])
    r0 = 1.225
    w = 2000
    h = L / N
    c = 343

    M = np.zeros((N, N), np.float32)
    K = np.zeros((N, N), np.float32)
    b = np.zeros((N,), np.float32)

    for i in range(N - 1):
            M[i:i + 2, i:i + 2] += m
            K[i:i + 2, i:i + 2] += k

    b[0] = -1.
    b[-1] = 0.

    A = (r0 * (c ** 2) / h * K) - ((w**2) * r0 * h / 6. * M)
    b = -1j * w * ((r0 * c)**2) * b
    print(b)

    x = np.linalg.solve(A, b)
    print(x)
    p = np.repeat(np.expand_dims(x, axis=0).imag, 10, axis=0)
    plt.imshow(p, cmap='hot', interpolation='nearest')
    plt.show()

