#                              .-----.
#                             /7  .  (
#                            /   .-.  \
#                           /   /   \  \
#                          / `  )   (   )
#                         / `   )   ).  \
#                       .'  _.   \_/  . |
#      .--.           .' _.' )`.        |
#     (    `---...._.'   `---.'_)    ..  \
#      \            `----....___    `. \  |
#       `.           _ ----- _   `._  )/  |
#         `.       /"  \   /"  \`.  `._   |
#           `.    ((O)` ) ((O)` ) `.   `._\
#             `-- '`---'   `---' )  `.    `-.
#                /                  ` \      `-.
#              .'                      `.       `.
#             /                     `  ` `.       `-.
#      .--.   \ ===._____.======. `    `   `. .___.--`     .''''.
#     ' .` `-. `.                )`. `   ` ` \          .' . '  8)
#    (8  .  ` `-.`.               ( .  ` `  .`\      .'  '    ' /
#     \  `. `    `-.               ) ` .   ` ` \  .'   ' .  '  /
#      \ ` `.  ` . \`.    .--.     |  ` ) `   .``/   '  // .  /
#       `.  ``. .   \ \   .-- `.  (  ` /_   ` . / ' .  '/   .'
#         `. ` \  `  \ \  '-.   `-'  .'  `-.  `   .  .'/  .'
#           \ `.`.  ` \ \    ) /`._.`       `.  ` .  .'  /
#            |  `.`. . \ \  (.'               `.   .'  .'
#         __/  .. \ \ ` ) \                     \.' .. \__
#  .-._.-'     '"  ) .-'   `.                   (  '"     `-._.--.
# (_________.-====' / .' /\_)`--..__________..-- `====-. _________)
#                  (.'(.'

import numpy as np
import sympy as sp
import matplotlib.pyplot as plt

np.set_printoptions(formatter={'float': '{: 0.3f}'.format}, linewidth=500)


def base_func_0(x, L, N):
    rv = np.zeros((1, N), dtype=np.float32)
    for i in range(1, N // 2 + 1):
        rv[0, i - 1] = np.sin(x * i * np.pi / (2 * L))
    for i in range(N // 2 + 1, N + 1):
        rv[0, i - 1] = np.cos(x * (i - N // 2) * np.pi / (2 * L))
    return rv


def base_func_1(x, L, N):
    rv = np.zeros((1, N), dtype=np.float32)
    for i in range(1, N // 2 + 1):
        rv[0, i - 1] = np.cos(x * i * np.pi / (2 * L)) * i * np.pi / (2 * L)
    for i in range(N // 2 + 1, N + 1):
        rv[0, i - 1] = - np.sin(x * (i - N // 2) * np.pi / (2 * L)) * (i - N // 2) * np.pi / (2 * L)
    return rv


def base_func_2(x, L, N):
    rv = np.zeros((1, N), dtype=np.float32)
    for i in range(1, N // 2 + 1):
        rv[0, i - 1] = - np.sin(x * i * np.pi / (2 * L)) * (i * np.pi / (2 * L)) ** 2
    for i in range(N // 2 + 1, N + 1):
        rv[0, i - 1] = - np.cos(x * (i - N // 2) * np.pi / (2 * L)) * ((i - N // 2) * np.pi / (2 * L)) ** 2
    return rv


def base_func_3(x, L, N):
    rv = np.zeros((1, N), dtype=np.float32)
    for i in range(1, N // 2 + 1):
        rv[0, i - 1] = - np.cos(x * i * np.pi / (2 * L)) * (i * np.pi / (2 * L)) ** 3
    for i in range(N // 2 + 1, N + 1):
        rv[0, i - 1] = np.sin(x * (i - N // 2) * np.pi / (2 * L)) * ((i - N // 2) * np.pi / (2 * L)) ** 3
    return rv


if __name__ == "__main__":
    L = 5
    N = 10

    A = np.zeros((N + 4, N + 4), dtype=np.float32)
    b = np.zeros((N + 4), dtype=np.float32)

    A1 = np.matmul(base_func_0(L, L, N).T,
                   base_func_3(L, L, N)) - np.matmul(base_func_0(0, L, N).T,
                                                     base_func_3(0, L, N))
    A2 = (np.matmul(base_func_1(L, L, N).T,
                    base_func_2(L, L, N)) - np.matmul(base_func_1(0, L, N).T,
                                                      base_func_2(0, L, N))) * -1.

    A3 = np.zeros((N, N), dtype=np.float32)
    for r in range(N):
        for t in range(N):
            x = sp.Symbol('x')
            if r < N // 2:
                i = r + 1
                f1 = - sp.sin(i * np.pi * x / (2 * L)) * (i * np.pi / (2 * L)) ** 2
            else:
                i = r + 1
                f1 = - sp.cos((i - N // 2) * np.pi * x / (2 * L)) * ((i - N // 2) * np.pi / (2 * L)) ** 2
            if t < N // 2:
                i = t + 1
                f2 = - sp.sin(i * np.pi * x / (2 * L)) * (i * np.pi / (2 * L)) ** 2
            else:
                i = t + 1
                f2 = - sp.cos((i - N // 2) * np.pi * x / (2 * L)) * ((i - N // 2) * np.pi / (2 * L)) ** 2

            A3[r, t] = sp.integrate(f1 * f2, (x, 0, L))

    Ac = np.zeros((4, N + 4), dtype=np.float32)
    Ac[0, :N] = base_func_0(0, L, N)[0, :]
    Ac[1, :N] = base_func_1(0, L, N)[0, :]
    Ac[2, :N] = base_func_2(L, L, N)[0, :]
    Ac[3, :N] = base_func_3(L, L, N)[0, :]

    for r in range(N):
        x = sp.Symbol('x')
        if r < N // 2:
            i = r + 1
            f1 = sp.sin(x * i * np.pi / (2 * L))
        else:
            i = r + 1
            f1 = sp.cos(x * (i - N // 2) * np.pi / (2 * L))
        f2 = 80. * sp.DiracDelta(x - 1.25) - 10.0 * sp.DiracDelta(x - 4.9)
        # f2 = 1.0 * sp.DiracDelta(x - 1) # - 6.0 * sp.DiracDelta(x - 4.5)

        b[r] = sp.integrate(f1 * f2, (x, 0, L))

    E = 79.
    I = 0.1 #0.5 ** 3 / 12
    A[:N, :N] = (A1 + A2 + A3) * E * I
    A[N:, :] = Ac
    A[:, N:] = Ac.T

    print(A)
    print("------------------------------------------------------------------------")
    print(b)
    print("------------------------------------------------------------------------")
    x = np.linalg.solve(A, b)
    print(x)

    q = x[:N]

    samples = 100
    x = np.linspace(0, L, num=samples)
    y = np.zeros((samples,), dtype=np.float32)
    for i in range(samples):
        y[i] = np.matmul(base_func_0(x[i], L, N), np.expand_dims(q, axis=1))
    axes = plt.gca()
    axes.set_xlim([0, 5.0])
    axes.set_ylim([-10, 10])
    plt.plot(x, y)
    plt.show()

    # samples = 100
    # x = np.linspace(0, L, num=samples)
    # y = np.zeros((samples,N), dtype=np.float32)
    # for i in range(samples):
    #     y[i, :] = base_func_1(x[i], L, N)[0]
    # axes = plt.gca()
    # axes.set_xlim([0, 5.0])
    # axes.set_ylim([-1, 1])
    # for i in range(N//2):
    #     plt.plot(x, y[:, i])
    # plt.show()
    # for i in range(N//2, N):
    #     plt.plot(x, y[:, i])
    # plt.show()
